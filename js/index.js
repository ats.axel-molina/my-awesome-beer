const weekdays = {
    MON: 0,
    TUE: 1,
    WED: 2,
    THU: 3,
    FRI: 4,
    SAT: 5,
    SUN: 6
}
const timeFrame = {
    PARTY: 0,
    NO: 1,
    WEIRD: 2,
    HAPPY: 3,
    YEAH: 4
}

const messages = new Map();
let actualDay = weekdays.MON;
let actualTimeFrame = timeFrame.YEAH;

function initMessages() {

    messages[weekdays.MON] = [[timeFrame.PARTY = ["No seas banquinista"],
        timeFrame.NO = ["No seas banquinista"],
        timeFrame.WEIRD = ["No seas banquinista"],
        timeFrame.HAPPY = ["No seas banquinista"],
        timeFrame.YEAH = ["No seas banquinista"]]];
    messages[weekdays.TUE] = [[timeFrame.PARTY = ["No deberias, TENES que tomar"],
        timeFrame.NO = ["No seas banquinista"],
        timeFrame.WEIRD = ["el almuerzo lo merece", "una de postre"],
        timeFrame.HAPPY = ["NO VISTE QUE ES HAPPY HOUR?", "Deberias ir por la tercera", "Tu hermana", "Deberias estar yendo a un happy", "Dale que se acaba el happy"],
        timeFrame.YEAH = ["https://www.youtube.com/watch?v=t35X84y8iMU"]]];
    messages[weekdays.WED] = [[timeFrame.PARTY = ["No deberias, TENES que tomar", "Acordate del laburo"],
        timeFrame.NO = ["No seas banquinista"],
        timeFrame.WEIRD = ["el almuerzo lo merece", "una de postre"],
        timeFrame.HAPPY = ["NO VISTE QUE ES HAPPY HOUR?", "Deberias ir por la tercera", "Tu hermana", "Deberias estar yendo a un happy", "Dale que se acaba el happy"],
        timeFrame.YEAH = ["https://www.youtube.com/watch?v=t35X84y8iMU"]]];
    messages[weekdays.THU] = [[timeFrame.PARTY = ["No deberias, TENES que tomar"],
        timeFrame.NO = ["No seas banquinista"],
        timeFrame.WEIRD = ["el almuerzo lo merece", "una de postre"],
        timeFrame.HAPPY = ["NO VISTE QUE ES HAPPY HOUR?", "Deberias ir por la tercera", "Tu hermana", "Deberias estar yendo a un happy", "Dale que se acaba el happy"],
        timeFrame.YEAH = ["https://www.youtube.com/watch?v=t35X84y8iMU"]]];
    messages[weekdays.FRI] = [[timeFrame.PARTY = ["No deberias, TENES que tomar"],
        timeFrame.NO = ["No seas banquinista"],
        timeFrame.WEIRD = ["el almuerzo lo merece", "una de postre"],
        timeFrame.HAPPY = ["NO VISTE QUE ES HAPPY HOUR?", "Deberias ir por la tercera", "Tu hermana", "Deberias estar yendo a un happy", "Dale que se acaba el happy"],
        timeFrame.YEAH = ["https://www.youtube.com/watch?v=t35X84y8iMU"]]];
    messages[weekdays.SAT] = [[timeFrame.PARTY = ["No deberias, TENES que tomar"],
        timeFrame.NO = ["No seas banquinista"],
        timeFrame.WEIRD = ["el almuerzo lo merece", "una de postre"],
        timeFrame.HAPPY = ["NO VISTE QUE ES HAPPY HOUR?", "Deberias ir por la tercera", "Tu hermana", "Deberias estar yendo a un happy", "Dale que se acaba el happy"],
        timeFrame.YEAH = ["https://www.youtube.com/watch?v=t35X84y8iMU"]]];
    messages[weekdays.SUN] = [[timeFrame.PARTY = ["No deberias, TENES que tomar"],
        timeFrame.NO = ["No seas banquinista"],
        timeFrame.WEIRD = ["el almuerzo lo merece", "una de postre"],
        timeFrame.HAPPY = ["NO VISTE QUE ES HAPPY HOUR?", "Deberias ir por la tercera", "Tu hermana", "Deberias estar yendo a un happy", "Dale que se acaba el happy"],
        timeFrame.YEAH = ["https://www.youtube.com/watch?v=t35X84y8iMU"]]];
}

function setMessage() {


    var d = new Date();
    var n = d.getDay();


    let messageElementElement = messages[actualDay][0][actualTimeFrame];
    console.log("actual "+ [0])

    document.getElementById("message").innerHTML = messageElementElement[getRandomPosition(messageElementElement.length)]
}


/* MAPS api*/
let map;


function getRandomPosition(l) {
    return Math.floor(Math.random() * l);
}


function setTimeFrame() {
    var d = new Date(); // current time
    var currentHours = d.getHours();
    actualDay = d.getDay();
    switch (currentHours) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
            actualTimeFrame = timeFrame.PARTY;
            break;
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
            actualTimeFrame = timeFrame.NO;
            break;
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
            actualTimeFrame = timeFrame.WEIRD;
            break;
        case 18:
        case 19:
        case 20:
        case 21:
            actualTimeFrame = timeFrame.HAPPY;
            break;
        case 22:
        case 23:
        case 24:
            actualTimeFrame = timeFrame.YEAH;
            break;
        default:
            actualTimeFrame = timeFrame.YEAH;
            break;
    }
}


function initMap() {
    if (navigator.geolocation) { //check if geolocation is available
        navigator.geolocation.getCurrentPosition(function (position) {
            map = new google.maps.Map(document.getElementById("map"), {
                center: {lat: position.coords.latitude, lng: position.coords.longitude},
                zoom: 17,
            });
        });
    }

}

setTimeFrame();
initMessages();
setMessage();


